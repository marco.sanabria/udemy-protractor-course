const environment = require('./environment');

module.exports = {
  browserName: 'chrome',
  maxInstances: 3,
  version: environment.getChromeVersion(),
  shardTestFiles: true,
  chromeOptions: {
    args: [
      'no-sandbox',
      '--disable-dev-shm-usage',
      '--headless'
    ]
  },
  metadata: {
    browser: {
      name: 'chrome',
      version: environment.getChromeVersion()
    },
    device: 'Docker',
    platform: {
      name: environment.getOSName(),
      version: environment.getOSVersion()
    }
  },
  loggingPrefs: {
    driver: 'ALL',
    server: 'ALL',
    browser: 'ALL'
  }
}
