import { promise } from 'selenium-webdriver';
import { ElementFinder } from 'protractor/built/element';
import { BotBase } from './bots.bot-base';
import { browser, protractor } from 'protractor';

export class UserBot extends BotBase {

  constructor() {
    super();
  }

  /**
   * Enters text in a given text box
   * @param {ElementFinder} element Web element mapping the text box
   * @param {string} text The specified text to type in the text box
   * @return {promise.Promise<void>} Promise that will be resolved with a void
   */
  public async typeIn(element: ElementFinder, text: string): promise.Promise<void> {
    await this.scrollTo(element);
    await element.clear();
    return await element.sendKeys(text);
  }

  /**
   * Performs the action mousing hover a section of the view
   * @param {ElementFinder} element Web Element that maps the section to mouse hover
   * @returns {promise.Promise<void>} Promise that will be resolved with a void
   */
  public async mouseHover(element: ElementFinder): promise.Promise<void> {
    await this.scrollTo(element);
    return await browser
      .actions()
      .mouseMove(element.getWebElement())
      .perform();
  }

  /**
   * Performs enter key press
   * @returns {promise.Promise<void>} Promise that will be resolved with a void
   */
  public async hitEnter(): promise.Promise<void> {
    return browser
      .actions()
      .sendKeys(protractor.Key.ENTER)
      .perform();
  }

  /**
   * Performs escape key press
   * @returns {promise.Promise<void>} Promise that will be resolved with a void
   */
  public async hitEscape(): promise.Promise<void> {
    return browser
      .actions()
      .sendKeys(protractor.Key.ESCAPE)
      .perform();
  }

  /**
   * Performs a click action on a given web element
   * @param {ElementFinder} element The web element to click on
   * @returns {promise.Promise<void>} Promise that will be resolved with a void
   */
  public async clickOn(element: ElementFinder): promise.Promise<void> {
    await this.waitUntilVisible(element);
    await this.scrollTo(element);
    return await element.click();
  }

  /**
   * Performs a click action on a given web element using Actions
   * @param {ElementFinder} element The web element to click on
   * @returns {Promise<void>} Promise that will be resolved with a void
   */
  public async actionsClickOn(element: ElementFinder): Promise<void> {
    await this.scrollTo(element);
    return await browser.actions().mouseMove(element).click().perform();
  }

  /**
   * Chooses a given option from a select element
   * @param {ElementFinder} selectElement The select dropdown to select from
   * @param {string} option The option to be selected
   * @returns {promise.Promise<void>} Promise that will be resolved with a void
   */
  public async chooseFromSelect(selectElement: ElementFinder, option: string): promise.Promise<void> {
    await this.clickOn(selectElement);
    await this.waitFor(2);
    return await this.clickOn(this.findOptionInSelect(selectElement, option));
  }

  /**
   * Quits the browser
   * @returns {promise.Promise<void>} Promise that will be resolved with a void
   */
  public async quitBrowser(): promise.Promise<void> {
    return browser.quit();
  }

  /**
   * Finds a specific option within the available options in a select element
   * @param {ElementFinder} selectElement The select element to choose an option from
   * @param {string} option Name of the searched option
   * @returns {ElementFinder} The searched option within the select
   */
  private findOptionInSelect(selectElement: ElementFinder, option: string): ElementFinder {
    return selectElement
      .$$('option')
      .filter(selectOption => selectOption.getText().then(text => text.trim() === option))
      .first();
  }
}
