const chrome = require('./src/config/capabilities/chrome');
const firefox = require('./src/config/capabilities/firefox');
const fs = require('fs');
const path = require('path');
const rimraf = require('rimraf');
const mkdirp = require('mkdirp');
const reportsDir = path.join(process.cwd(), './reports/protractor-e2e');
const jsonReportsDir = path.join(process.cwd(), './reports/protractor-e2e/json');
const targetOutputDir = reportsDir + '/e2e';

function createDirectory(dir, empty = false) {
  if (!fs.existsSync(dir)) {
    mkdirp.sync(dir);
  } else if (empty) {
    rimraf(dir + '/*', () => console.log('emptied ' + dir));
  }
}

createDirectory(jsonReportsDir);
createDirectory(targetOutputDir);

exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  baseUrl: '',
  framework: 'jasmine',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  SELENIUM_PROMISE_MANAGER: true,
  multiCapabilities: [ chrome, firefox ],
  params: {},
  specs: ['./src/features/**/*.feature'],
  defaultTimeout: 90000,
  getPageTimeout: 90000,
  allScriptsTimeout: 90000,
  restartBrowserBetweenTests: false,
  ignoreUncaughtExceptions: true,
  cucumberOpts: {
    compiler: 'ts:ts-node/register',
    format: ['json:' + jsonReportsDir + '/cucumber_report.json'],
    colors: true,
    backtrace: true,
    tags: ['~@not-run'],
    require: [
      './src/steps/**/*.steps.ts',
      './src/core/support/hooks.ts'
    ]
  },

  onPrepare: function onPrepare() {
    browser.ignoreSynchronization = true;
    browser.driver.manage().window().setSize(1650, 1000);
    require('ts-node').register({
      project: path.join(__dirname, './tsconfig.json')
    });
  },

  afterLaunch: function (exitCode) {
    console.log('Exit code is:', exitCode);
    exitCode === 0 ? process.exit(0) : process.exit(1);
  }
};
